class CreateResearches < ActiveRecord::Migration[6.1]
  def change
    create_table :researches do |t|
      t.string :name
      t.text :description
      t.references :algorithm
      t.timestamps
    end
  end
end
