class AddLanguagesFileToResearches < ActiveRecord::Migration[6.1]
  def change
    add_column :researches, :languages_file, :string
  end
end
