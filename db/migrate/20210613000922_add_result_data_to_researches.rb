class AddResultDataToResearches < ActiveRecord::Migration[6.1]
  def change
    add_column :researches, :result_data, :string
  end
end
