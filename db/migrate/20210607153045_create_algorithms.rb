class CreateAlgorithms < ActiveRecord::Migration[6.1]
  def change
    create_table :algorithms do |t|
      t.string :name
      t.string :code_name
      t.timestamps
    end
  end
end
