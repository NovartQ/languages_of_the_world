#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime
from scipy.cluster.hierarchy import linkage, dendrogram
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("filepath", nargs='?', help="Input file of languages for the analysis")
args = parser.parse_args()
filepath = args.filepath

languages_set = pd.read_csv("lib/python_scripts/languages_data.tsv",'\t')

if args.filepath is None:
    languages_names = list(languages_set.pop('Языки'))
    languages_vectors = languages_set.values[1:]
    dict_langs = dict(zip(languages_names, languages_vectors))
else:
    my_file = open(args.filepath, "r")
    languages_names = my_file.read().splitlines()
    languages_vectors = languages_set.values[1:]
    # pre-fil dictionary of investigated languages with an empty array
    dict_langs = { i : [] for i in languages_names }
    for l in languages_vectors:
      if l[0] in languages_names:
        dict_langs[l[0]] = l[1:]

mergings = linkage(list(dict_langs.values()), method='complete')
plt.rcParams["figure.figsize"] = (20,7)
dendrogram(mergings,
           labels=list(dict_langs.keys()),
           leaf_rotation=90,
           leaf_font_size=6,
          )
plt.subplots_adjust(bottom=0.35) # to adjust it for pdf view
date = datetime.now().strftime("%d%m%Y%H%M%S")
filepath = '/tmp/hierarchical_{date}.pdf'.format(date=date)
plt.savefig(filepath)
print(filepath)
# plt.show()
