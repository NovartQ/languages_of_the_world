class ResearchesController < ApplicationController
  before_action :set_research, only: %i[show destroy]
  before_action :set_algorithms, only: %i[index new show create]

  def index
    @researches = Research.all
  end

  def new
    @research = Research.new
  end

  def show; end

  def create
    @research = Research.new(research_params)

    if @research.save
      optional_file = "public#{@research.languages_file.url}" if @research.languages_file.url.present?
      filepath = `python3 lib/python_scripts/#{@research.algorithm.code_name} #{optional_file}`.strip
      filename = filepath.split('/').last
      File.open(filepath, 'r') do |f|
        @research.result_data = f
        @research.save
        send_data f.read.force_encoding('BINARY'), filename: filename, type: 'application/pdf', disposition: 'attachment'
      end
      File.delete(filepath)
    else
      render :new, danger: 'Возникла ошибка при создании исследования'
    end
  end

  def destroy
    @research.destroy
    redirect_to researches_path, warning: 'Запись была успешно удалена'
  end

  private

  def set_research
    @research = Research.find(params[:id])
  end

  def set_algorithms
    @algorithms = Algorithm.order(:name)
  end

  def research_params
    params.require(:research).permit(:name, :description, :languages_file, :algorithm_id)
  end
end
