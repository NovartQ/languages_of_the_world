class Research < ApplicationRecord
  belongs_to :algorithm
  mount_uploader :result_data, ResultDataUploader
  mount_uploader :languages_file, LanguagesFileUploader

  validates_presence_of :name

end
